import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 * Kelas ini bertugas untuk melakukan enkripsi dan dekripsi dengan Algoritma AES
 * memanfaatkan library javax.crypto
 */
public class AES {

    /**
     * Method untuk enkripsi dengan metode AES
     * @param plaintext Plaintext
     * @param key Key dengan panjang 16-bit HEX
     * @return Ciphertext dengan panjang sesuai Plaintext
     */
    public static byte[] encrypt(byte[] plaintext, String key) {
        try {
            SecretKey secretKey = new SecretKeySpec(DatatypeConverter.parseHexBinary(key), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] result = cipher.doFinal(plaintext);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method untuk dekripsi dengan metode AES
     * @param ciphertext Ciphertext
     * @param key Key dengan panjang 16-bit HEX
     * @return Plaintext dengan panjang sesuai Ciphertext
     */
    public static byte[] decrypt(byte[] ciphertext, String key) {
        try {
            SecretKey secretKey = new SecretKeySpec(DatatypeConverter.parseHexBinary(key), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] result = cipher.doFinal(ciphertext);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
