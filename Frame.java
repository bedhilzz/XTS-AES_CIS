import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Kelas yang bertugas untuk menampilkan GUI
 */
public class Frame extends JFrame {

    public static void main(String[] args) {
        Frame frame = new Frame();
        frame.setVisible(true);
    }

    private JPanel panel;
    private JTabbedPane tabbedPane;
    private JLayeredPane encryptionLayeredPane;
    private JLayeredPane decryptionLayeredPane;
    private JLabel firstFilePathEncrypt;
    private JLabel firstFilePathDecrypt;
    private JLabel secondFilePathEncrypt;
    private JLabel secondFilePathDecrypt;
    private JLabel plainTextLabel;
    private JLabel cipherTextLabel;
    private JLabel keyEncryptLabel;
    private JLabel keyDecryptLabel;
    private JButton browsePlainTextButton;
    private JButton browseCipherTextButton;
    private JButton browseKeyEncryptButton;
    private JButton browseKeyDecryptButton;
    private JButton encryptButton;
    private JButton decryptButton;
    private File firstFile;
    private File secondFile;
    public static final int FRAME_WIDTH = 640;
    public static final int FRAME_HEIGHT = 280;
    public static final int LABEL_WIDTH = 100;
    public static final int LABEL_HEIGHT = 30;
    public static final int PATH_LABEL_WIDTH = 400;
    public static final int BUTTON_WIDTH = 100;
    public static final int BUTTON_HEIGHT = 30;
    public static final int EXECUTE_BUTTON_WIDTH = 240;
    public static final int EXECUTE_BUTTON_HEIGHT = 50;
    public static final int FIRST_ROW_POSITION = 20;
    public static final int FIRST_COL_POSITION = 20;
    public static final int SECOND_ROW_POSITION = 60;
    public static final int SECOND_COL_POSITION = 120;
    public static final int THIRD_ROW_POSITION = 120;
    public static final int THIRD_COL_POSITION = 520;
    public static final int EXECUTE_BUTTON_POSITION = 200;

    public Frame() {
        setTitle("XTS-AES");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setLocationRelativeTo(null);
        setResizable(false);
        createPanel();
        createTabbedPane();
        createLayeredPane();
        createFileInput();
        createFileInputInfo();
        createBrowseFileButton();
        createExecuteButton();
    }

    public void createPanel() {
        panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.setBackground(new Color(9, 113, 178));
        setContentPane(panel);
        panel.setLayout(null);
    }

    public void createTabbedPane() {
        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        panel.add(tabbedPane);
    }

    public void createLayeredPane() {
        encryptionLayeredPane = new JLayeredPane();
        decryptionLayeredPane = new JLayeredPane();
        encryptionLayeredPane.setOpaque(true);
        decryptionLayeredPane.setOpaque(true);
        encryptionLayeredPane.setBackground(new Color(20, 133, 204));
        decryptionLayeredPane.setBackground(new Color(20, 133, 204));
        tabbedPane.addTab("Encryption", encryptionLayeredPane);
        tabbedPane.addTab("Decryption", decryptionLayeredPane);
    }

    public void createFileInput() {
        plainTextLabel = new JLabel("Plain Text");
        plainTextLabel.setBounds(FIRST_COL_POSITION, FIRST_ROW_POSITION, LABEL_WIDTH, LABEL_HEIGHT);
        encryptionLayeredPane.add(plainTextLabel);

        cipherTextLabel = new JLabel("Cipher Text");
        cipherTextLabel.setBounds(FIRST_COL_POSITION, FIRST_ROW_POSITION, LABEL_WIDTH, LABEL_HEIGHT);
        decryptionLayeredPane.add(cipherTextLabel);

        keyEncryptLabel = new JLabel("Key Encrypt");
        keyEncryptLabel.setBounds(FIRST_COL_POSITION, SECOND_ROW_POSITION, LABEL_WIDTH, LABEL_HEIGHT);
        encryptionLayeredPane.add(keyEncryptLabel);

        keyDecryptLabel = new JLabel("Key Decrypt");
        keyDecryptLabel.setBounds(FIRST_COL_POSITION, SECOND_ROW_POSITION, LABEL_WIDTH, LABEL_HEIGHT);
        decryptionLayeredPane.add(keyDecryptLabel);
    }

    public void createFileInputInfo() {
        firstFilePathEncrypt = new JLabel("(no file selected)");
        firstFilePathEncrypt.setBounds(SECOND_COL_POSITION, FIRST_ROW_POSITION, PATH_LABEL_WIDTH, LABEL_HEIGHT);
        encryptionLayeredPane.add(firstFilePathEncrypt);

        secondFilePathEncrypt = new JLabel("(no file selected)");
        secondFilePathEncrypt.setBounds(SECOND_COL_POSITION, SECOND_ROW_POSITION, PATH_LABEL_WIDTH, LABEL_HEIGHT);
        encryptionLayeredPane.add(secondFilePathEncrypt);

        firstFilePathDecrypt = new JLabel("(no file selected)");
        firstFilePathDecrypt.setBounds(SECOND_COL_POSITION, FIRST_ROW_POSITION, PATH_LABEL_WIDTH, LABEL_HEIGHT);
        decryptionLayeredPane.add(firstFilePathDecrypt);

        secondFilePathDecrypt = new JLabel("(no file selected)");
        secondFilePathDecrypt.setBounds(SECOND_COL_POSITION, SECOND_ROW_POSITION, PATH_LABEL_WIDTH, LABEL_HEIGHT);
        decryptionLayeredPane.add(secondFilePathDecrypt);
    }

    public void createBrowseFileButton() {
        browsePlainTextButton = new JButton("Browse");
        browseCipherTextButton = new JButton("Browse");
        browseKeyEncryptButton = new JButton("Browse");
        browseKeyDecryptButton = new JButton("Browse");

        browsePlainTextButton.setBounds(THIRD_COL_POSITION, FIRST_ROW_POSITION, BUTTON_WIDTH, BUTTON_HEIGHT);
        browseKeyEncryptButton.setBounds(THIRD_COL_POSITION, SECOND_ROW_POSITION, BUTTON_WIDTH, BUTTON_HEIGHT);

        browseCipherTextButton.setBounds(THIRD_COL_POSITION, FIRST_ROW_POSITION, BUTTON_WIDTH, BUTTON_HEIGHT);
        browseKeyDecryptButton.setBounds(THIRD_COL_POSITION, SECOND_ROW_POSITION, BUTTON_WIDTH, BUTTON_HEIGHT);

        encryptionLayeredPane.add(browsePlainTextButton);
        encryptionLayeredPane.add(browseKeyEncryptButton);

        decryptionLayeredPane.add(browseCipherTextButton);
        decryptionLayeredPane.add(browseKeyDecryptButton);

        browsePlainTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile(1, true);
            }
        });
        browseKeyEncryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile(2, true);
            }
        });

        browseCipherTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile(1, false);
            }
        });
        browseKeyDecryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile(2, false);
            }
        });
    }

    public void chooseFile(int fileNumber, boolean encrypt) {
        final JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showOpenDialog(encryptionLayeredPane);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            String path = file.getAbsolutePath();
            if (fileNumber == 1) {
                firstFile = file;
                if (encrypt) {
                    firstFilePathEncrypt.setText(path);
                } else {
                    firstFilePathDecrypt.setText(path);
                }
            } else if (fileNumber == 2) {
                secondFile = file;
                if (encrypt) {
                    secondFilePathEncrypt.setText(path);
                } else  {
                    secondFilePathDecrypt.setText(path);
                }
            }
        }
    }

    public String saveFile() {
        final JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile().getAbsolutePath();
        }
        return null;
    }

    public void createExecuteButton() {
        encryptButton = new JButton("ENCRYPT");
        decryptButton = new JButton("DECRYPT");

        encryptButton.setBounds(
                EXECUTE_BUTTON_POSITION,
                THIRD_ROW_POSITION,
                EXECUTE_BUTTON_WIDTH,
                EXECUTE_BUTTON_HEIGHT
        );

        decryptButton.setBounds(
                EXECUTE_BUTTON_POSITION,
                THIRD_ROW_POSITION,
                EXECUTE_BUTTON_WIDTH,
                EXECUTE_BUTTON_HEIGHT
        );

        encryptionLayeredPane.add(encryptButton);
        decryptionLayeredPane.add(decryptButton);

        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                encrypt();
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrypt();
            }
        });
    }

    public void showInvalidFileDialog() {
        JOptionPane.showMessageDialog(
                null,
                "File cannot be empty",
                "Please choose files...",
                JOptionPane.WARNING_MESSAGE
        );
    }

    public void showInvalidKeyDialog() {
        JOptionPane.showMessageDialog(
                null,
                "Key must be 64-bits of HEX length!",
                "Invalid Key",
                JOptionPane.WARNING_MESSAGE
        );
    }

    public void encrypt() {
        try {
            if (filesChecked()) {
                FileReader fileReader = new FileReader(secondFile.getAbsoluteFile());
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                String key = bufferedReader.readLine();
                if (XTS.isValid(key)) {
                    // TODO: encrypt
                    String destinationPath = saveFile();
                    byte[][] result = XTS.encrypt(firstFile, key);

                    Util.write(destinationPath, result);

                    JOptionPane.showMessageDialog(
                            null,
                            "File encryption success!",
                            "Encryption Success",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                } else {
                    showInvalidKeyDialog();
                }
            } else {
                showInvalidFileDialog();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decrypt() {
        try {
            if (filesChecked()) {
                FileReader fileReader = new FileReader(secondFile.getAbsoluteFile());
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                String key = bufferedReader.readLine();
                if (XTS.isValid(key)) {
                    // TODO: encrypt
                    String destinationPath = saveFile();
                    byte[][] result = XTS.decrypt(firstFile, key);

                    Util.write(destinationPath, result);

                    JOptionPane.showMessageDialog(
                            null,
                            "File decryption success!",
                            "Decryption Success",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                } else {
                    showInvalidKeyDialog();
                }
            } else {
                showInvalidFileDialog();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean filesChecked() {
        return (firstFile != null || secondFile != null);
    }
}
