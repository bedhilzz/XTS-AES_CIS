import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class Util {
    public static final char[] HEX_DIGITS = {
            '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'
    };

    /**
     * Method untuk membaca tiap byte di dalam file
     * @param file
     * @return Array of bytes dimana dimensi pertama menyimpan satu blok
     */
    public static byte[][] read(File file) {
        byte[][] result = new byte[XTS.numberOfBlocks][XTS.BLOCK_SIZE];
        try {
            InputStream inputStream = new FileInputStream(file);

            for (int i = 0; i < result.length; i++) {
                inputStream.read(result[i]);
            }

            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method untuk menulis hasil enkripsi/dekripsi ke dalam file
     * @param destinationPath Destination path file yang ingin ditulis
     * @param data Data yang ingin dituliskan ke dalam file
     */
    public static void write(String destinationPath, byte[][] data) {
        try {
            RandomAccessFile rw = new RandomAccessFile(destinationPath, "rw");
            for (int i = 0; i < data.length; i++) {
                for(int j=0; j < data[i].length; j++)
                    rw.write(data[i][j]);
            }
            rw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Credits to Lawrie Brown, Oct 2001
     *
     * @param hex String of hex(es)
     * @return Array of bytes
     */
    public static byte[] hexToByte(String hex) {
        int length = hex.length();
        if (hex.isEmpty() || hex.length() < 2) {
            return null;
        } else {
            byte[] result = new byte[length/2];
            for (int i = 0; i < length/2; i++) {
                    result[i] = (byte) Integer.parseInt(hex.substring(i * 2, i * 2 + 2), 16);
            }
            return result;
        }
    }

    /**
     * Credits to Lawrie Brown, Oct 2001
     *
     * @param bytes Array of bytes
     * @return String of hex(es)
     */
    public static String byteToHex(byte[] bytes) {
        int length = bytes.length;
        char[] buf = new char[length * 2];
        for (int i = 0, j = 0, k; i < length; ) {
            k = bytes[i++];
            buf[j++] = HEX_DIGITS[(k >>> 4) & 0x0F];
            buf[j++] = HEX_DIGITS[ k        & 0x0F];
        }
        return new String(buf);
    }
}
