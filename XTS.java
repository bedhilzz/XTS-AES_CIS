import java.io.*;
import java.util.Arrays;

/**
 * Kelas ini bertugas untuk melakukan algroritma XTS.
 *
 * Kelas ini memanfaatkan algoritma enkripsi/dekripsi
 * untuk tiap blok AES dari kelas AES
 */
public class XTS {
    public static String firstKey;
    public static String secondKey;
    public static final int BLOCK_SIZE = 16;
    public static final int KEY_LENGTH = 64;
    public static int numberOfBlocks;
    private static int padding;
    public static final String nonce = "86321156753486462135643383138651";
    private static byte[][] tweaks;

    public static boolean isValid(String key) {
        return key.matches("[a-fA-F0-9]{64}");
    }

    public static void setUp(File file, String key) {
        firstKey = key.substring(0, KEY_LENGTH/2);
        secondKey = key.substring(KEY_LENGTH/2);

        numberOfBlocks = (int) Math.ceil(file.length()/BLOCK_SIZE);
        padding = (int) (file.length() % BLOCK_SIZE);

        generateTweaks();
    }

    /**
     * Method untuk melakukan enkripsi XTS
     * @param plaintext File Plaintext
     * @param key Key
     * @return Ciphertext hasil enkripsi
     */
    public static byte[][] encrypt(File plaintext, String key) {
        setUp(plaintext, key);
        byte[][] plaintextByte = Util.read(plaintext);
        byte[][] result = new byte[numberOfBlocks][BLOCK_SIZE];

        /**
         * Lakukan enkripsi AES sampai blok terakhir
         */
        for (int i = 0; i < numberOfBlocks - 1; i++) {
            result[i] = encryptBlock(plaintextByte[i], i);
        }

        if (padding == 0) {
            result[numberOfBlocks - 1] = encryptBlock(plaintextByte[numberOfBlocks - 1], numberOfBlocks - 1);
        } else {
            // TODO : Lakukan stealing
            byte[] block = new byte[BLOCK_SIZE];
            for (int i = 0; i < BLOCK_SIZE; i++) {
                block[i] = result[numberOfBlocks - 2][i];
            }
            byte[] steal = getSteal(block);
            byte[] lastBlock = appendSteal(plaintextByte[numberOfBlocks - 1], steal);
            result[numberOfBlocks - 2] = encryptBlock(lastBlock, numberOfBlocks - 1);
            result[numberOfBlocks - 1] = getStealRemainder(block);
        }
        return result;
    }

    /**
     * Method untuk melakukan dekripsi XTS
     * @param cipherText File Ciphertext
     * @param key Key
     * @return Plaintext hasil dekripsi
     */
    public static byte[][] decrypt(File cipherText, String key) {
        setUp(cipherText, key);
        byte[][] cipherTextByte = Util.read(cipherText);
        byte[][] result = new byte[numberOfBlocks][BLOCK_SIZE];

        /**
         * Lakukan dekripsi AES sampai blok terakhir
         */
        for (int i = 0; i < numberOfBlocks - 1; i++) {
            result[i] = decryptBlock(cipherTextByte[i], i);
        }

        if (padding == 0) {
            result[numberOfBlocks - 1] = decryptBlock(cipherTextByte[numberOfBlocks - 1], numberOfBlocks - 1);
        } else {
            // TODO : Lakukan stealing
            byte[] block = new byte[BLOCK_SIZE];
            for (int i = 0; i < BLOCK_SIZE; i++) {
                block[i] = result[numberOfBlocks - 2][i];
            }
            byte[] steal = getSteal(block);
            byte[] lastBlock = appendSteal(cipherTextByte[numberOfBlocks - 1], steal);
            result[numberOfBlocks - 2] = decryptBlock(lastBlock, numberOfBlocks - 1);
            result[numberOfBlocks - 1] = getStealRemainder(block);
        }
        return result;
    }

    public static byte[] getSteal(byte[] block) {
        byte[] steal = new byte[BLOCK_SIZE - padding];
        for (int i = padding; i < BLOCK_SIZE; i++) {
            steal[i - padding] = block[i];
        }
        return steal;
    }

    public static byte[] getStealRemainder(byte[] block) {
        byte[] stealRemainder = new byte[padding];
        for (int i = 0; i < padding; i++) {
            stealRemainder[i] = block[i];
        }
        return stealRemainder;
    }

    public static byte[] appendSteal(byte[] source, byte[] steal) {
        byte[] result = new byte[BLOCK_SIZE];
        int sourceLength = source.length;
        for (int i = 0; i < sourceLength; i++) {
            result[i] = source[i];
        }
        for (int i = sourceLength; i < BLOCK_SIZE; i++) {
            result[i] = steal[i - sourceLength];
        }
        return result;
    }

    public static byte[] encryptBlock(byte[] block, int blockNum) {
        byte[] tweak = tweaks[blockNum];

        byte[] result = xor16(tweak, block);
        result = AES.encrypt(result, firstKey);
        result = xor16(tweak, result);
        return result;
    }

    public static byte[] decryptBlock(byte[] block, int blockNum) {
        byte[] tweak = tweaks[blockNum];

        byte[] result = xor16(tweak, block);
        result = AES.decrypt(result, firstKey);
        result = xor16(tweak, result);
        return result;
    }

    public static byte[] xor16(byte[] a, byte[] b) {
        byte[] result = new byte[BLOCK_SIZE];
        for(int i=0; i<a.length; i++){
            result[i] = (byte)(a[i]^b[i]);
        }
        return result;
    }

    /**
     * Method ini bertfungsi untuk menghasilkan tweaks setiap blok.
     */
    private static void generateTweaks() {
        tweaks = new byte[numberOfBlocks + 1][BLOCK_SIZE];
        tweaks[0] = AES.encrypt(Util.hexToByte(nonce), secondKey);
        for (int i = 1; i < numberOfBlocks + 1; i++) {
            tweaks[i][0] = (byte) ((2 * (tweaks[i-1][0] % 128)) ^ (135 * (tweaks[i-1][15] / 128)));
            for (int j = 1; j < 16; j++) {
                tweaks[i][j] = (byte) ((2 * (tweaks[i-1][j] % 128)) ^ (tweaks[i-1][j-1] / 128));
            }
        }
    }
}
